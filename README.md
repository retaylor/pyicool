# pyicool

Python wrapper to edit text files of icool, run icool.exe, produce the optics of the output and compare the optics over the cells.
Also includes functions to have a basic optimiser of icool variables, and a multi-threaded option to reduce run time.