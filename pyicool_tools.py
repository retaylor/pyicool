import subprocess
import re

def RunICool(Dir):
    print("---------------------")
    print(f"RUNNING ICOOL.exe")
    os.chdir(Dir)
    !icool.exe
    os.chdir('..')
    print("---------------------")   

def FOR001(Dir, Type, val):
    '''
    Description: Substitutes the for001.dat file with a new parameter.
    Requires:
    Dir  : Directory where the FOR001.dat file is saved
    Type : The parameter to set, e.g. rfGRAD1, rfFREQ1
    val  :  value that this parameter is set to.
    '''

    var = ''
    print("---------------------")
    print(f"LOADING {Type} = {val}")

    X = open(f"{Dir}/for001.dat")
    #Finding the line in for001 to substitute.
    for line in X:
        line = line.strip()
        if line.startswith(f"&SUB  {Type}"):
            var = line
    X.close()

    X = open(f"{Dir}/for001.dat")
    #Substituting the line.
    x_content = (re.sub(var, f"&SUB  {Type}    {val}", X.read()))
    X.close()
    
    #Write contents to file.
    #Using mode 'w' truncates the file.
    X = open(f"{Dir}/for001.dat", 'w')
    X.write(x_content)
    print(f"for001 SAVED")
    print("---------------------")
    X.close()
    
    
def ecalc(Dir, Type, val):
    '''Saves the final values of perpendicular, longitudinal and 6D emittance to a text file.
       Returns luminosity so that the calculation of optimal value can continue.
       
       Dir   :  Directory where ecalc9f.exe is
       Type  :  Variable changed in this iteration
       val   :  Value of the variable'''
    #Runs the ecalc executable
    os.chdir(Dir)
    !ecalc9f.exe
    os.chdir(ParDir)
    
    #Reading results from ecalc
    ECalc = open(f"{Dir}/ecalc9f.dat")
    ECalc_Data = ECalc.read()
    ECalc.close()

    #Saving the ecalc file under a new name
    ECalc_new= open(f"{Dir}/ecalc_{Type}_{val}.dat", 'w')
    ECalc_new.write(ECalc_Data)
    ECalc_new.close()
            
    #Finding number of good particles in for002
    Particles = open(f"{Dir}/for002.dat")
    for line in Particles:
        line = line.lstrip()
        if line.startswith("Total number of final good particles"):
            print(line)
            good = [int(s) for s in line if s.isdigit()]
            Good = sum(d * 10**i for i, d in enumerate(good[::-1]))

    Particles.close()
            
    E = np.loadtxt(f"{Dir}/ecalc9f.dat", skiprows=13)
    e_p = E[1:,3]
    e_l = E[1:,4]
    e_6d = E[1:,5]
    
    E_f = e_6d[-1] #Selecting the final emittance
    #E_f = np.amin(e_6d) #Selecting the smallest emittance
    #n = np.argmin(e_6d)
    N_Particles = 3000
    T = Good/N_Particles
    L = T**2 / np.sqrt(E_f)
    
    Y = open(f"{Dir}\..\Emit_{Type}_{val}.txt", 'a+')
    Y.write('Type   : value e_p     e_l        e_6d       particles  Luminosity \n')
    Y.write(f"{Type}: {val}  {e_p[-1]:.2e}   {e_l[-1]:.2e}   {e_6d[-1]:.2e} {Good} {L:.2f}\n")
    Y.close()
    return L